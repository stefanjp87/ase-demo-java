package at.ac.tuwien.ase.labass19;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Labass19Application {

	public static void main(String[] args) {
		SpringApplication.run(Labass19Application.class, args);
	}

}
