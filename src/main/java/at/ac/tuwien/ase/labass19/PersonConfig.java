package at.ac.tuwien.ase.labass19;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

@SpringBootConfiguration
public class PersonConfig {
    @Bean
    public ResourceConfig jerseyConfig() {
        return new ResourceConfig()
                .packages("at.ac.tuwien.ase.labass19");
    }
    @Bean
    public PersonResource greetingResource() {
        return new PersonResource();
    }
    @Bean
    public PersonService greetingService() {
        return new PersonService();
    }
}
