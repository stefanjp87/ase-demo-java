package at.ac.tuwien.ase.labass19;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.Optional;
@Path("/")
public class PersonResource {

    @Inject
    private PersonService personService;

    @GET
    @Path("/persons")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGreeting() {
        List<Person> all = personService.getAll();
        return Response.ok().entity(all).build();
    }

    @POST
    @Path("/person")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addPerson(Person person) {
        personService.add(person);
        return Response.created(URI.create("/person")).entity(person).build();
    }

    @PUT
    @Path("/person/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response changePerson(Person person, @PathParam("id") Long id) {
        Person result = personService.changePerson(id, person);
        if(result != null) {
            return Response.ok().entity(result).build();
        } else {
            return Response.status(404).build();
        }
    }

    @GET
    @Path("/person/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPerson(@PathParam("id") Long id) {
        Optional<Person> p = personService.get(id);
        if(p.isPresent()) {
            return Response.ok().entity(p.get()).build();
        } else {
            return Response.status(404).build();
        }
    }

    @GET
    @Path("/person/alternative")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPersonFromHeader(@HeaderParam("id") Long id) {
        Optional<Person> p = personService.get(id);
        if(p.isPresent()) {
            return Response.ok().entity(p.get()).build();
        } else {
            return Response.status(404).build();
        }
    }

    @DELETE
    @Path("/person/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePerson(@PathParam("id") Long id) {
        boolean success = personService.delete(id);
        if(success) {
            return Response.noContent().build();
        } else {
            return Response.status(404).build();
        }
    }
}
