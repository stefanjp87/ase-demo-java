package at.ac.tuwien.ase.labass19;

import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PersonService {
    @Autowired
    private IPersonRepository greetingRepository;

    public void add(Person person) {
        greetingRepository.save(person);
    }

    public Optional<Person> get(Long id) {
        Optional<Person> p = greetingRepository.findById(id);
        return p;
    }

    public List<Person> getAll() {
        List<Person> ret = new ArrayList<>();
        Iterable<Person> all = greetingRepository.findAll();
        all.forEach(ret::add);
        return ret;
    }

    public Person changePerson(Long id, Person person) {
        Optional<Person> p = greetingRepository.findById(id);
        if(p.isPresent()) {
            p.get().setFirstName(person.getFirstName());
            p.get().setLastName(person.getLastName());
            Person result = p.get();
            greetingRepository.save(result);
            return result;
        } else {
            return null;
        }
    }

    public boolean delete(Long id) {
        Optional<Person> p = greetingRepository.findById(id);
        if(p.isPresent()) {
            greetingRepository.delete(p.get());
            return true;
        } else {
            return false;
        }
    }
}
