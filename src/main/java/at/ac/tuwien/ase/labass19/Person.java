package at.ac.tuwien.ase.labass19;

import com.fasterxml.jackson.annotation.JsonGetter;
import org.springframework.data.annotation.Id;
public class Person {

    private Long id;
    private String firstName;
    private String lastName;

    public Person(Long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Id
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @JsonGetter("first-name")
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    @JsonGetter("last-name")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public static Person createPerson(String firstName, String lastName) {
        return new Person(null, firstName, lastName);
    }
}
