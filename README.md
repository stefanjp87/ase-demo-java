# ASE-Demo-Java

Program to store and access persons and their addresses.

## Person

### View all persons

**Definition**

`GET /persons`

**Response**

- `200 OK` on success

```json
[{
  "id": "1",
  "first-name": "max",
  "last-name": "huber"
},
{
  "id": "2",
  "first-name": "franziska",
  "last-name": "berger"
},
{
  "id": "3",
  "first-name": "hubert",
  "last-name": "von goisern"
}]
```

### Create a person

```
POST /person

{
  "first-name": "max",
  "last-name": "huber"
}
```
**Response**

- `201 Created` if person has been created successfully

### Change a person

```
PUT /person/<id>

{
  "first-name": "max",
  "last-name": "huber"
}
```
**Response**

- `200 Ok` if person could be changed
- `404 Not Found` if person does not exist


### Get info about a person

**Definition**

```
GET /person/<id>

```

alternative

```
GET /person/alternative
id=1

```

**Response**

- `200 OK` on success

```json
{
  "id": "3",
  "first-name": "hubert",
  "last-name": "von goisern"
}
```

- `404 Not Found` if person does not exist


### Delete a person

**Definition**

```
DELETE /person/<id>
```

**Response**

- `204 No Content` if person could be deleted
- `404 Not Found` if person could not be foundok